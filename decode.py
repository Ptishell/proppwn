#! /usr/bin/python

import sys
import struct

CODE_SIZE = 959
COPYRIGHT_SIZE = 28
RUNNER_SIZE = 36

WORD_SIZE = 4

bitmap = [19, 16,  8, 31, 25, 12, 27, 30, 21, 11, 0, 20, 28, 5, 18, 22,
          13, 24, 10, 26, 23, 29,  6, 15,  2,  9, 1,  4,  7, 3, 17, 14]


def decode(instr):
    res = 0
    instr = struct.unpack("<I", instr)[0]
    for i in range(32):
        bit = instr & 1
        res |= bit << bitmap[i]
        instr >>= 1
    return struct.pack("<I", res)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("USGAE: {0} INPUT OUTPUT".format(sys.argv[0]))
        exit(2)
    raw = open(sys.argv[1], 'rb')
    output = open(sys.argv[2], 'wb+')
    raw.seek(2)
    for i in range(CODE_SIZE):
        output.write(decode(raw.read(WORD_SIZE)))
    for i in range(COPYRIGHT_SIZE):
        output.write(raw.read(WORD_SIZE))
    for i in range(RUNNER_SIZE):
        output.write(raw.read(WORD_SIZE))
    raw.close()
    output.close()
