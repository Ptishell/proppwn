# PropPwn

## 32-bit mapping (ROM)

    Instruction Bit : 31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16
    Encoded ROM Bit :  3  7 21 12  6 19  4 17 20 15  8 11  0 14 30  1

    Instruction Bit : 15 14 13 12 11 10  9  8  7  6  5  4  3  2  1  0
    Encoded ROM Bit : 23 31 16  5  9 18 25  2 28 22 13 27 29 24 26 10

(http://forums.parallax.com/showthread.php/101336-Code-protect?p=710858&viewfull=1#post710858)
