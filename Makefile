DEV ?= /dev/ttyACM0
SERIAL ?= /dev/ttyUSB0

BSTC = bstc
BFLAGS = -p0 -d $(DEV) -L lib
BSTL = bstl

MINICOM = minicom
MFLAGS = -D $(SERIAL) -w -b 9600
STTY = stty
DD = dd

BOOTLOADER_RAW = bootloader_raw.bin
BOOTLOADER = bootloader.bin
BOOTSIZE = 4094

MAIN = main.spin
DUMP = dump.py
DECODE = decode.py

all::
	$(BSTC) $(BFLAGS) $(MAIN)

check::
	$(BSTL) -d $(DEV)

serial::
	$(MINICOM) $(MFLAGS)

dump::
	./$(DUMP) $(SERIAL) $(BOOTLOADER_RAW)
	./$(DECODE) $(BOOTLOADER_RAW) $(BOOTLOADER)
	most $(BOOTLOADER)

clean::
	$(RM) $(BOOTLOADER)
