#! /usr/bin/python

import sys
import serial

OPCODE_HELLO        = "h"
OPCODE_DUMP_ROM     = "r"
OPCODE_DUMP_CHARSET = "c"
OPCODE_DUMP_LOG     = "l"
OPCODE_DUMP_ANTILOG = "a"
OPCODE_DUMP_SINE    = "s"
OPCODE_DUMP_BOOT    = "b"

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("USAGE: {0} DEVICE OUTPUT".format(sys.argv[0]))
        exit(2)
    serial = serial.Serial(sys.argv[1], 9600)
    serial.write(OPCODE_HELLO.encode())
    print(serial.readline().decode())
    serial.write(OPCODE_DUMP_BOOT.encode())
    length = int(serial.readline().decode())
    f = open(sys.argv[2], 'wb+')
    f.write(serial.read(length))
    f.close()
