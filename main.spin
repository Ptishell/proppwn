OBJ
    pin: "io"
    time: "timing"
    serial: "serial"

VAR
    LONG AnimStack[16]
    LONG DumpStack[16]

CON
    _clkmode            = RCFAST
    _clkfreq            = 13_000_000
    BAUDRATE            = 9600

    CHARSET_START       = $8000
    CHARSET_STOP        = $BFFF

    LOG_START           = $C000
    LOG_STOP            = $CFFF

    ANTILOG_START       = $CFFF
    ANTILOG_STOP        = $DFFF

    SINE_START          = $E000
    SINE_STOP           = $F001

    BOOTLOADER_START    = $F002
    BOOTLOADER_STOP     = $FFFF

    OPCODE_HELLO        = "h"
    OPCODE_DUMP_ROM     = "r"
    OPCODE_DUMP_CHARSET = "c"
    OPCODE_DUMP_LOG     = "l"
    OPCODE_DUMP_ANTILOG = "a"
    OPCODE_DUMP_SINE    = "s"
    OPCODE_DUMP_BOOT    = "b"

PUB Main | c
    serial.Start(BAUDRATE)
    serial.Str(string("Ready", $D, $A))
    COGNEW(Anim, @AnimStack)
    REPEAT
        CASE serial.CharIn
            OPCODE_HELLO        : serial.Str(string("Hello World !", $D, $A))
            OPCODE_DUMP_ROM     : Dump(CHARSET_START, BOOTLOADER_STOP)
            OPCODE_DUMP_CHARSET : Dump(CHARSET_START, CHARSET_STOP)
            OPCODE_DUMP_LOG     : Dump(LOG_START, LOG_STOP)
            OPCODE_DUMP_ANTILOG : Dump(ANTILOG_START, ANTILOG_STOP)
            OPCODE_DUMP_SINE    : Dump(SINE_START, SINE_STOP)
            OPCODE_DUMP_BOOT    : Dump(BOOTLOADER_START, BOOTLOADER_STOP)

PUB Anim | i, dir
    i := 1
    dir := TRUE
    REPEAT
        IF pin.In(16)
            pin.Outs(15, 8, $FF)
        ELSE
            pin.Outs(15, 8, i)
            IF dir
                i := i << 1
            ELSE
                i := i >> 1
            IF i == $80 OR i == 1
                dir := !DIR
            time.Pause(50)


PUB Dump(begin, end) | i
    serial.Dec(end - begin + 1)
    serial.Str(string($A))
    REPEAT i FROM begin TO end
        serial.Char(BYTE[i])
