from idaapi import *




class DecodingError(Exception):
    pass


class ParallaxPropeller(processor_t):
    id = 0x8000 + 5857
    flag = PR_ADJSEGS | PRN_HEX | PR_WORD_INS
    cnbits = 32
    dnbits = 8
    psnames = ["paraprop"]
    plnames = ["Parallax Propeller"]
    segreg_size = 0
    instruc_start = 0

    assembler = {"flag": AS_NCHRE | ASH_HEXF4 | ASD_DECF1 | ASO_OCTF3
                 | ASB_BINF2 | AS_NOTAB,
                 "uflag": 0,
                 "name": "Parallax Propeller assembler",
                 "origin": ".org",
                 "end": ".end",
                 "cmnt": ";",
                 "ascsep": '"',
                 "accsep": "'",
                 "esccodes": "\"'",
                 "a_ascii": ".ascii",
                 "a_byte": ".byte",
                 "a_word": ".word",
                 "a_bss": "dfs %s",
                 "a_seg": "seg",
                 "a_curip": "PC",
                 "a_public": "",
                 "a_weak": "",
                 "a_extrn": ".extern",
                 "a_comdef": "",
                 "a_align": ".align",
                 "lbrace": "(",
                 "rbrace": ")",
                 "a_mod": "%",
                 "a_band": "&",
                 "a_bor": "|",
                 "a_xor": "^",
                 "a_bnot": "~",
                 "a_shl": "<<",
                 "a_shr": ">>",
                 "a_sizeof_fmt": "size %s",
                 }

    regNames = reg_names = ["R" + str(x) for x in range(0x1F0)] + \
                           ["PAR", "CNT", "INA", "INA", "INB",
                            "OUTA", "OUTB", "DIRA", "DIRB",
                            "CRTA", "CTRB", "FRQA", "FRQB",
                            "PHSA", "PHSB", "VCFG", "VSCL",
                            "CS", "DS"]

    basicInstr = [
            {'name': 'ABS',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'ABSNEG',     'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'ADD',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'ADDABS',     'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'ADDS',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'ADDSX',      'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'ADDX',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'AND',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'ANDN',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'CALL',       'feature': CF_USE1 | CF_CALL},
            {'name': 'CLKSET',     'feature': CF_USE1},
            {'name': 'CMP',        'feature': CF_USE1 | CF_USE2},
            {'name': 'CMPS',       'feature': CF_USE1 | CF_USE2},
            {'name': 'CMPSUB',     'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'CMPSX',      'feature': CF_USE1 | CF_USE2},
            {'name': 'CMPX',       'feature': CF_USE1 | CF_USE2},
            {'name': 'COGID',      'feature': CF_USE1 | CF_CHG1},
            {'name': 'COGINIT',    'feature': CF_USE1},
            {'name': 'COGSTOP',    'feature': CF_USE1},
            {'name': 'DJNZ',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'HUBOP',      'feature': CF_USE1 | CF_USE2},
            {'name': 'JMP',        'feature': CF_USE1},
            {'name': 'JMPRET',     'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'LOCKCLR',    'feature': CF_USE1},
            {'name': 'LOCKNEW',    'feature': CF_USE1 | CF_CHG1},
            {'name': 'LOCKRET',    'feature': CF_USE1},
            {'name': 'LOCKSET',    'feature': CF_USE1},
            {'name': 'MAX',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'MAXS',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'MIN',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'MINS',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'MOV',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'MOVD',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'MOVI',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'MOVS',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'MUXC',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'MUXNC',      'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'MUXNZ',      'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'MUXZ',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'NEG',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'NEGC',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'NEGNC',      'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'NEGNZ',      'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'NEGZ',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'NOP',        'feature': 0},
            {'name': 'OR',         'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'RCL',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'RCR',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'RDBYTE',     'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'RDLONG',     'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'RDWORD',     'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'RET',        'feature': CF_STOP},
            {'name': 'REV',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'ROL',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'ROR',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SAR',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SHL',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SHR',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SUB',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SUBABS',     'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SUBS',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SUBSX',      'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SUBX',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SUMC',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SUMNC',      'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SUMNZ',      'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'SUMZ',       'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'TEST',       'feature': CF_USE1 | CF_USE2},
            {'name': 'TESTN',      'feature': CF_USE1 | CF_USE2},
            {'name': 'TJNZ',       'feature': CF_USE1 | CF_USE2},
            {'name': 'TJZ',        'feature': CF_USE1 | CF_USE2},
            {'name': 'WAITCNT',    'feature': CF_USE1 | CF_USE2 | CF_CHG2},
            {'name': 'WAITPEQ',    'feature': CF_USE1 | CF_USE2},
            {'name': 'WAITPNE',    'feature': CF_USE1 | CF_USE2},
            {'name': 'WAITVID',    'feature': CF_USE1 | CF_USE2},
            {'name': 'WRBYTE',     'feature': CF_USE1 | CF_USE2},
            {'name': 'WRLONG',     'feature': CF_USE1 | CF_USE2},
            {'name': 'WRWORD',     'feature': CF_USE1 | CF_USE2},
            {'name': 'XOR',        'feature': CF_USE1 | CF_USE2 | CF_CHG2},
    ]

    opcodes = {
            0b101010: 'ABS',
            0b101011: 'ABSNEG',
            0b100000: 'ADD',
            0b100010: 'ADDABS',
            0b110100: 'ADDS',
            0b110110: 'ADDSX',
            0b110010: 'ADDX',
            0b011000: 'AND',
            0b011001: 'ANDN',
            0b100001: 'CMP',
            0b110000: 'CMPS',
            0b111000: 'CMPSUB',
            0b111001: 'CMPSX',
            0b111011: 'CMPX',
            0b111001: 'DJNZ',
            0b000011: 'HUBOP',
            0b010111: 'JMP',
            0b010011: 'MAX',
            0b010001: 'MAXS',
            0b010010: 'MIN',
            0b010000: 'MINS',
            0b101000: 'MOV',
            0b010101: 'MOVD',
            0b010110: 'MOVI',
            0b010100: 'MOVS',
            0b011100: 'MUXC',
            0b011101: 'MUXNC',
            0b011111: 'MUXNZ',
            0b011110: 'MUXZ',
            0b101001: 'NEG',
            0b101100: 'NEGC',
            0b101101: 'NEGNC',
            0b101111: 'NEGNZ',
            0b101110: 'NEGZ',
            0b011010: 'OR',
            0b001101: 'RCL',
            0b001100: 'RCR',
            0b000000: 'MMBYTE',
            0b000010: 'MMLONG',
            0b000001: 'MMWORD',
            0b001111: 'REV',
            0b001001: 'ROL',
            0b001000: 'ROR',
            0b001110: 'SAR',
            0b001011: 'SHL',
            0b001010: 'SHR',
            0b100011: 'SUBABS',
            0b110101: 'SUBS',
            0b110111: 'SUBSX',
            0b110011: 'SUBX',
            0b100100: 'SUMC',
            0b100101: 'SUMNC',
            0b100111: 'SUMNZ',
            0b100110: 'SUMZ',
            0b011000: 'TEST',
            0b011001: 'TESTN',
            0b111010: 'TJNZ',
            0b111011: 'TJZ',
            0b111110: 'WAITCNT',
            0b111100: 'WAITPEQ',
            0b111101: 'WAITPNE',
            0b111111: 'WAITVID',
            0b011011: 'XOR',
    }

    opcode_hubop = {
            0b000: 'CLKSET',
            0b001: 'COGID',
            0b010: 'COGINIT',
            0b011: 'COGSTOP',
            0b111: 'LOCKCLR',
            0b100: 'LOCKNEW',
            0b101: 'LOCKRET',
            0b110: 'LOCKSET',
    }

    opcode_cond = {
            0b1111: "      ",
            0b0000: "      ",
            0b1010: "E     ",
            0b0101: "NE    ",
            0b0001: "A     ",
            0b1100: "B     ",
            0b0011: "AE    ",
            0b1110: "BE    ",
            0b1001: "C=Z   ",
            0b0110: "C!=Z  ",
            0b1000: "C.Z   ",
            0b0100: "C.NZ  ",
            0b0010: "NC.Z  ",
            0b1101: "C+NZ  ",
            0b1011: "NC+Z  ",
            0b0111: "NC+NZ ",
    }


    OPCODE_MASK_INSTR   = 0xFC000000
    OPCODE_OFFSET_INSTR = 26
    OPCODE_MASK_ZCRI    = 0x03C00000
    OPCODE_OFFSET_ZCRI  = 22
    OPCODE_MASK_CON     = 0x003C0000
    OPCODE_OFFSET_CON   = 18
    OPCODE_MASK_DEST    = 0x0003FE00
    OPCODE_OFFSET_DEST  = 9
    OPCODE_MASK_SRC     = 0x000001FF
    OPCODE_OFFSET_SRC  = 0

    def __init__(self):
        processor_t.__init__(self)
        self._init_instructions()
        self._init_registers()

    def _init_instructions(self):
        self.instruc = []
        for i in self.basicInstr:
            for opcode, mnem in self.opcode_cond.iteritems():
                feature = i['feature']
                if (i['name'] == "JMP" and opcode == 0):
                    feature |= CF_STOP
                self.instruc.append({'name': mnem + i['name'],
                                     'feature': feature})
        self.instrs = self.instruc
        self.instruc_end = len(self.instruc)
        self.inames = {}
        for idx, ins in enumerate(self.instruc):
            self.inames[ins['name']] = idx

    def _init_registers(self):
        self.reg_ids = {}
        for i, reg in enumerate(self.regNames):
            self.reg_ids[reg] = i
        self.regFirstSreg = self.regCodeSreg = self.reg_ids["CS"]
        self.regLastSreg = self.regDataSreg = self.reg_ids["DS"]

    def _read_cmd_byte(self):
        byte = get_full_byte(ea)
        self.cmd.size += 1
        return byte

    def _ana(self):
        cmd = self.cmd

        ea = cmd.ea + cmd.size
        o = struct.unpack('<I', get_many_bytes(ea, 4))[0]
        self.cmd.size += 1
        opcode = {
            'instr': (o & self.OPCODE_MASK_INSTR) >> self.OPCODE_OFFSET_INSTR,
            'zcri': (o & self.OPCODE_MASK_ZCRI) >> self.OPCODE_OFFSET_ZCRI,
            'con': (o & self.OPCODE_MASK_CON) >> self.OPCODE_OFFSET_CON,
            'dest': (o & self.OPCODE_MASK_DEST) >> self.OPCODE_OFFSET_DEST,
            'src': (o & self.OPCODE_MASK_SRC) >> self.OPCODE_OFFSET_SRC,
        }
        mnem = "UNKNOWN"

        if opcode['con'] == 0b0000:
            mnem = "NOP"
        else:
            try:
                mnem = self.opcodes[opcode['instr']]
            except KeyError:
                raise DecodingError()
            cmd[0].type = o_reg
            cmd[0].reg = opcode['dest']
            if mnem[:2] == "MM":
                if opcode['zcri'] & 1:
                    cmd[1].type = o_mem
                    cmd[1].addr = opcode['src']
                else:
                    cmd[1].type = o_phrase
                    cmd[1].reg = opcode['src']
                if (opcode['zcri'] >> 1) & 1:
                    mnem = "RD" + mnem[2:]
                else:
                    mnem = "WR" + mnem[2:]
            elif mnem == "HUBOP":
                try:
                    mnem = self.opcode_hubop[opcode['src'] & 0b111]
                except KeyError:
                    pass
            elif mnem == "JMP":
                if opcode['zcri'] & 1:
                    cmd[0].type = o_near
                    cmd[0].addr = opcode['src']
                    if (opcode['zcri'] >> 1) == 0b001:
                        mnem = "CALL"
                    elif (opcode['src'] == 0 and opcode['dest'] == 0):
                        mnem = "RET"
                else:
                    cmd[0].type = o_reg
                    cmd[0].reg = opcode['src']
                    if (opcode['zcri'] >> 1) == 0b001:
                        mnem = "JMPRET"
            elif mnem in ["DJNZ", "TJNZ", "TJZ"]:
                if opcode['zcri'] & 1:
                    cmd[1].type = o_near
                    cmd[1].addr = opcode['src']
                else:
                    cmd[1].type = o_reg
                    cmd[1].reg = opcode['src']
            else:
                if opcode['zcri'] & 1:
                    cmd[1].type = o_imm
                    cmd[1].value = opcode['src']
                else:
                    cmd[1].type = o_reg
                    cmd[1].reg = opcode['src']
                if mnem == "CMP" and opcode['zcri'] >> 1 == 0b001:
                    mnem = "SUB"
        mnem = self.opcode_cond[opcode['con']] + mnem
        cmd.itype = self.inames[mnem]
        return cmd.size

    def ana(self):
        try:
            return self._ana()
        except DecodingError:
            return 0

    def _emu_operand(self, op):
        if op.type == o_mem:
            ua_dodata2(0, op.addr, op.dtyp)
            ua_add_dref(0, op.addr, dr_R)
        elif op.type == o_near:
            if self.cmd.get_canon_feature() & CF_CALL:
                fl = fl_CN
            else:
                fl = fl_JN
            ua_add_cref(0, op.addr, fl)

    def emu(self):
        cmd = self.cmd
        ft = cmd.get_canon_feature()
        if ft & CF_USE1:
            self._emu_operand(cmd[0])
        if ft & CF_USE2:
            self._emu_operand(cmd[1])
        if ft & CF_USE3:
            self._emu_operand(cmd[2])
        if not ft & CF_STOP:
            ua_add_cref(0, cmd.ea + cmd.size, fl_F)
        return True

    def outop(self, op):
        if op.type == o_reg:
            out_register(self.reg_names[op.reg])
        elif op.type == o_imm:
            OutValue(op, OOFW_IMM)
        elif op.type in [o_near, o_mem]:
            ok = out_name_expr(op, op.addr, BADADDR)
            if not ok:
                out_tagon(COLOR_ERROR)
                OutLong(op.addr, 16)
                out_tagoff(COLOR_ERROR)
                QueueMark(Q_noName, self.cmd.ea)
        elif op.type == o_phrase:
            out_symbol('[')
            out_register(self.reg_names[op.reg])
            out_symbol(']')
        else:
            return False
        return True

    def out(self):
        cmd = self.cmd
        ft = cmd.get_canon_feature()
        buf = init_output_buffer(1024)
        OutMnem(15)
        if ft & CF_USE1:
            out_one_operand(0)
        if ft & CF_USE2:
            OutChar(',')
            OutChar(' ')
            out_one_operand(1)
        if ft & CF_USE3:
            OutChar(',')
            OutChar(' ')
            out_one_operand(2)
        term_output_buffer()
        cvar.gl_comm = 1
        MakeLine(buf)


def PROCESSOR_ENTRY():
    return ParallaxPropeller()
